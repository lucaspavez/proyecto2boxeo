# Proyecto Gtk Python 3

Programa de Boxeadores:

Se diseño un software para poder gestionar Luchadores de boxeo y proximos combates entre luchadores.
se utilizo el programa Glade junto con la libreria matplotlib, para poder darle una interfaz grafica a nuestro codigo orientado a objetos.

Al iniciar el programa se deberá escoger si ingresar como usuario o publico. 
El usuario puede ser un administrador o un referi los cuales tienen permisos asignados.
Si se escoge ingresar como usuario publico, solo tendra permiso a observar los catalogos.


al ingresar al programa, se encontrara frente a 5 opciones:

    1. Agregar Boxeadores:
        Aqui como lo dice el titulo, se agrega un nuevo luchador, definiendo 8 caracteristicas de el, como lo es:
        el nombre, peso, altura,edad,  combates ganados,perdidos, empatados y nacionalidad.
    
    2. Generara un Evento nuevo:
        aqui se le da al usuario la oportunidad de poder crear un evento de pelea, definiendo la fecha, el lugar,el referi, cantidad de round
        y los nombres de los participantes.
    
    3. Añadir un referi:
        El administrador podra registrar nuevos referis. Tendra que ingresar los siguentes datos: nombre, numero de peleas arbitradas, edad,
        y nacionalidad
        
    4. Catalogo de Boxeadores:
        aqui esta la lista de los boxeadores que se agregaron en la seccion 1 mencionanda anteriormente, ademas se puede editar y eliminar 
        desde esa misma ventana.
        
    5. Catalogo eventos:
        Se podra visualizar los eventos con todos los datos que se han ingresado anteriormente 
        
el pograma funciona gracias a la creacion de diccionarios json los cuales almacenan los datos que se le ingresan para poder ser reutilizados
al cerrar y abrir nuevamente el programa 
    
AUTORES: MANUEL AVILA , LUCAS PAVEZ