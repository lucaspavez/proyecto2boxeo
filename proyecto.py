import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import matplotlib.pyplot as plt


# Save Boxers in Json Files
def open_file():
	
    try:
        with open('boxeadores.json', 'r') as file:
            data = json.load(file)
        file.close()
        
    except IOError:
        data = []
        
    return data
    
    
def save_file(data):
	
    print("Save File")

    with open('boxeadores.json', 'w') as file:
        json.dump(data, file, indent=4)
        
    file.close()

# Save events in Json files
def open_files():
    try:
        with open('Eventos.json', 'r') as file:
            data = json.load(file)
        file.close()
        
    except IOError:
        data = []
        
    return data
    
    
def save_files(data):
    print("Save File")
    with open('Eventos.json', 'w') as file:
        json.dump(data, file, indent=4)
        
    file.close()


# Save Arbitros in Json Files
def open_files2():
    try:
        with open('Arbitros.json', 'r') as file:
            data = json.load(file)
        file.close()
        
    except IOError:
        data = []
        
    return data
    
    
def save_files2(data):
    print("Archivo Guardado")
    with open('Arbitros.json', 'w') as file:
        json.dump(data, file, indent=4)
        
    file.close()


# Grafico 
def grafico(nombre, x, y):	
    plt.bar(x, y, color = (0, 1, 0), align='center')
    plt.ylabel("Valores")
    plt.title('Estadisticas de ' + nombre)
    plt.suptitle(nombre)
    plt.show()


# ComboBox
def obtener_valor_combobox(combo):
	tree_iter = combo.get_active_iter()
	
	if tree_iter is not None:
		model = combo.get_model()
		seleccion = model[tree_iter][0]

	return seleccion


def llenar_combo(combo, peliadores):
	lista = Gtk.ListStore(str)

	for i in peliadores:
		lista.append([i])

	combo.set_model(lista)
	renderer_text = Gtk.CellRendererText()
	combo.pack_start(renderer_text, True)
	combo.add_attribute(renderer_text, "text", 0)


#ventana Ventana inicio
class Ventana_inicio():
	def __init__(self):
		print ("Bienvenido al Software de Boxeadores 1.0")
		print ("")
		print ("Seleccione una opcion")
		print ("")

		#Ventana
		self.builder = Gtk.Builder()
		self.builder.add_from_file("inicio.glade")
		window = self.builder.get_object("principal")
		window.connect("destroy", Gtk.main_quit)
		window.set_default_size (600,400)
		window.set_title("Inicio Sesion")

		#Botones
		self.boton_administrador = self.builder.get_object("admin")
		self.boton_administrador.connect("clicked", self.abrir_sesion)
		self.boton_usuario = self.builder.get_object("user")
		self.boton_usuario.connect("clicked", self.abrir_ventana)

		window.show_all()

	def abrir_sesion(self, btn=None):
		print("ingrese su usuario y contraseña")
		x = Ventana_inicio_sesion()

	def abrir_ventana (self, btn=None):
		print ("ingresando al modo usuario")
		Usuario = 'usuario'
		x = Menu_Principal(Usuario)
 
 
# ventana inicio sesion
class Ventana_inicio_sesion():
	def __init__(self):
			#ventana
			self.builder = Gtk.Builder()
			self.builder.add_from_file("inicio.glade")
			self.ventana2 = self.builder.get_object("inicio_sesion")
			self.ventana2.set_title("Inicio de sesion")
			self.ventana2.set_default_size(450,250)

			#botones
			self.boton_aceptar = self.builder.get_object("acepta")
			self.boton_aceptar.connect("clicked", self.boton_acept)

			#usuarios
			self.usuario = self.builder.get_object("usuario")
			self.contrasena = self.builder.get_object("contrasena")

			#respuesta
			self.Respuesta = self.builder.get_object("respuesta")

			self.ventana2.show_all()

	def boton_acept(self, event):
		print("Apreto el boton Aceptar")	
		Usuario = self.usuario.get_text()
		Contrasena = self.contrasena.get_text()

		if Usuario == 'Admin' and Contrasena == 'admin':
			t = Menu_Principal(Usuario)
			self.ventana2.destroy()

		if Usuario == 'Referi' and Contrasena == 'boxeo':
			t = Menu_Principal(Usuario)
			self.ventana2.destroy()

		else:
			self.Respuesta.set_text("Usuario o Contraseña incorrecto")
 
 
#Ventana Principal
class Menu_Principal():
	def __init__(self, Usuario):
		print ("Construyendo")
		self.usuario = Usuario
		#Ventana
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")
		window = self.builder.get_object("ventana")
		window.connect("destroy", Gtk.main_quit)
		window.set_default_size(600,400)
		window.set_title("Software Boxeo 1.0")
		#window.connect("destroy")
		#Botones
		self.button_open_dialog = self.builder.get_object("Addboxer")
		self.button_open_dialog.connect("clicked", self.open_dialog)
		self.button_open_dialog2 = self.builder.get_object ("Addevent")
		self.button_open_dialog2.connect("clicked", self.open_dialog2)
		self.button_anadir_arbitro = self.builder.get_object("add_arbitro")
		self.button_anadir_arbitro.connect("clicked", self.ventana_arbitro)
		self.button_open_window2 = self.builder.get_object ("Boxeadores")
		self.button_open_window2.connect("clicked", self.open_window2)
		self.button_catalogo_evento = self.builder.get_object("catalogo_evento")
		self.button_catalogo_evento.connect("clicked", self.open_window3)

		window.show_all()

	def open_dialog(self, btn=None):
		print ("Agregar Boxeadores")

		if (self.usuario != 'usuario'):
			d =DialogoBoxeadores()

		elif (self.usuario == 'usuario'):
			d = Ventana_permiso()

	def ventana_arbitro(self, btn=None):
		print("aqui se agregan los arbitros")

		if (self.usuario == 'Admin'):
			x = Ventana_referis()

		elif (self.usuario != 'Admin'):
			d = Ventana_permiso()

	def open_dialog2(self, btn=None):
		print ("Agregar Eventos")

		if(self.usuario != 'Admin'):
			d = Ventana_permiso()

		elif(self.usuario == 'Admin'):
			d = Eventos()

	def open_window2(self, btn=None):
		print ("Ver boxeadores")
		d = Catalogo(self.usuario)

	def open_window3(self, btn=None):
		print("Ver Eventos")
		d = Catalogo_Eventos(self.usuario)


#ventana de agregar boxeadores en una clase
class DialogoBoxeadores():
	def __init__(self):
		print("constructor")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")
		self.dialogo = self.builder.get_object("Datos_boxeadores")
		self.dialogo.show_all()
		self.button_cancel = self.builder.get_object("boton_cancelar")
		self.button_cancel.connect("clicked", self.button_cancel_clicked)
		self.button_ok = self.builder.get_object("boton_aceptar")
		self.button_ok.connect("clicked", self.button_ok_clicked)

		self.name = self.builder.get_object("nombre")
		self.peso = self.builder.get_object("peso")
		self.altura = self.builder.get_object("altura")
		self.edad = self.builder.get_object("edad")
		self.win = self.builder.get_object("win")
		self.lose = self.builder.get_object("lose")
		self.empate = self.builder.get_object("empate")
		self.pais = self.builder.get_object("nacionalidad")

	def button_ok_clicked (self, btn=None):
		nombre = self.name.get_text()
		peso = self.peso.get_text()
		altura = self.altura.get_text()
		edad = self.edad.get_text()
		victorias = self.win.get_text()
		derrotas = self.lose.get_text()
		empates = self.empate.get_text()
		nacionalidad = self.pais.get_text()

		j = {"Nombre": nombre,
				"Peso": peso,
				"Altura": altura,
				"Edad": edad,
				"Victorias": victorias,
				"Derrotas": derrotas,
				"Empates": empates,
				"Nacionalidad": nacionalidad
				}

		f = open_file()
		f.append(j)
		save_file(f)
		self.dialogo.destroy()

	def button_cancel_clicked(self, btn=None):
		self.dialogo.destroy()


#ventana de agregar Eventos en una clase
class Eventos():
	def __init__(self):
		print("*****EVENTOS*****")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")
		self.dialog = self.builder.get_object("Ventana2")
		self.dialog.show_all()

		self.calendario = self.builder.get_object("calendario")
		self.peleas = self.builder.get_object("numero_peleas")
		self.nombres1 = self.builder.get_object("luchador1")
		self.nombres2 = self.builder.get_object("luchador2")
		self.ciudades = self.builder.get_object("Ciudades")
		self.arbitros = self.builder.get_object("arbitro")

		self.button_cancel = self.builder.get_object("cancelar")
		self.button_cancel.connect("clicked", self.button_cancel_clicked)
		self.button_ok = self.builder.get_object("aceptar")
		self.button_ok.connect("clicked",self.button_ok_clicked)
		
		#Arbitros
		arbitros = []
		data = open_files2()
		
		for i in range(len(data)):
			arbitro = data[i]["Nombre Arbitro"]
			arbitros.append(arbitro)
			
		llenar_combo(self.arbitros, arbitros)

		#Luchadores
		luchadores1 = []
		data2 = open_file()
		
		for i in range(len(data2)):
			luchador = data2[i]["Nombre"]
			luchadores1.append(luchador)
			
		llenar_combo(self.nombres1, luchadores1)	
		
		luchadores2 = []
		data3 = open_file()
		
		for i in range(len(data3)):
			luchador = data3[i]["Nombre"]
			luchadores2.append(luchador)
			
		llenar_combo(self.nombres2, luchadores2)	

	def button_ok_clicked (self, btn=None):
		Peleas = self.peleas.get_text()
		Participante1 = obtener_valor_combobox(self.nombres1)
		Participante2 = obtener_valor_combobox(self.nombres2)
		fecha = self.calendario.get_date()
		lugar = self.ciudades.get_active_text()
		arbitro = obtener_valor_combobox(self.arbitros)

		j = { "Ano": fecha[0],
				"Mes": fecha[1],
				"Dia": fecha[2],
				"Ciudad": lugar,
				"Cantidad de peleas": Peleas,
				"Luchador1": Participante1,
				"Luchador2": Participante2,
				"Arbitro" : arbitro
				}

		f = open_files ()
		f.append(j)
		save_files(f)
		self.dialog.destroy()

	def button_cancel_clicked (self,btn=None):
		self.dialog.destroy()

#ventana en donde Muestra todos los boxeadores
class Catalogo():
	def __init__(self, Usuario):
		self.usuario = Usuario
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")
		windows = self.builder.get_object("ventana3")
		windows.set_default_size(800, 600)
		windows.set_title("CATALOGO LUCHADORES")

		#botones
		self.button_delete = self.builder.get_object("eliminar")
		self.button_delete.connect("clicked", self.delete_select_data)
		self.button_edit = self.builder.get_object("edit")
		self.button_edit.connect("clicked", self.edit_select_data)
		self.boton_info = self.builder.get_object("info")
		self.boton_info.connect("clicked", self.mostrar_grafico)

		#Muestra y ordena los resultados
		self.listmodel = Gtk.ListStore(str, str, str, str, str, str, str, str)
		self.treeResultado = self.builder.get_object("datos")
		self.treeResultado.set_model (model = self.listmodel)
		cell = Gtk.CellRendererText ()
		title = ("Nombre", "Peso", "Altura", "Edad", "Victorias", "Derrotas", "Empates", "Nacionalidad")
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			self.treeResultado.append_column(col)

		self.show_all_data()
		windows.show_all()

	def show_all_data(self):
		data = open_file()
		
		for i in data:
			x = [x for x in i.values()]
			self.listmodel.append(x)

	def mostrar_grafico(self, btn=None):
		model, it = self.treeResultado.get_selection().get_selected()
		
		if model is None or it is None:
			return

		nombre = model.get_value(it, 0)
		victorias = model.get_value(it, 4)
		empates = model.get_value(it, 6)
		derrotas = model.get_value(it, 5)


		y = [str(0), victorias, empates, derrotas]
		x = ["", "Victorias", "Empates", "Derrotas"]

		grafico(nombre, x, y)


	#metodo para eliminar una seccion
	def delete_select_data(self, btn=None):
		model, it = self.treeResultado.get_selection().get_selected()
		
		if model is None or it is None:
			return
			
		if (self.usuario == 'usuario'):
			d = Ventana_permiso ()
			
		elif (self.usuario != 'usuario'):
			data = open_file()
			
			for i in data:
				if(i['Nombre'] == model.get_value(it, 0)):
					data.remove(i)
					
			save_file(data)
			
			self.remove_all_data()
			self.show_all_data()

	def edit_select_data(self, btn =None):
		model, it = self.treeResultado.get_selection().get_selected()
		
		if model is None or it is None:
			return
			
		if (self.usuario == 'usuario'):
			d = Ventana_permiso ()
			
		elif (self.usuario != 'usuario'):
			
			d = DialogoBoxeadores()
			d.name.set_text(model.get_value(it, 0))
			d.peso.set_text(model.get_value(it, 1))
			d.altura.set_text(model.get_value(it, 2))
			d.edad.set_text(model.get_value(it, 3))
			d.win.set_text(model.get_value(it , 4))
			d.lose.set_text(model.get_value(it, 5))
			d.empate.set_text(model.get_value(it, 6))
			d.pais.set_text(model.get_value(it,7))
			response = d.dialogo.run()
			
			self.remove_all_data()
			self.show_all_data()

	def remove_all_data(self):
		if len (self.listmodel) != 0:
			for i in range(len(self.listmodel)):
				iter = self.listmodel.get_iter(0)
				self.listmodel.remove(iter)
				
		print ("Empty list")
		
		
#ventana donde muestra los eventos guardados
class Catalogo_Eventos():
	def __init__(self,Usuario):
		self.usuario = Usuario
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")
		windowss = self.builder.get_object("Ventana4")
		windowss.set_default_size(800, 600)
		windowss.set_title("CATALOGO EVENTOS")

		#Buttons
		self.button_eliminar = self.builder.get_object("Eliminar_evento")
		self.button_eliminar.connect("clicked", self.eliminar)

		self.listmodel = Gtk.ListStore(int, int, int, str, str, str, str, str)
		self.treePeleas = self.builder.get_object("peleas")
		self.treePeleas.set_model (model = self.listmodel)
		cell = Gtk.CellRendererText ()

		title = ("Año","Mes","Dia", "Lugar", "Rounds", "Luchador1", "Luchado2", "Arbitro")
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			self.treePeleas.append_column(col)

		self.show_all_data()
		windowss.show_all()

	def show_all_data(self):
		data = open_files()
		for i in data:
			x = [x for x in i.values()]
			self.listmodel.append(x)

	def eliminar(self, btn=None):
		model, it = self.treePeleas.get_selection().get_selected()
		
		if model is None or it is None:
			return
			
		if (self.usuario != 'Admin'):
			d = Ventana_permiso()
			
		elif(self.usuario == 'Admin' or self.usuario == 'Referi'):	
			data = open_files()
			
			for i in data:
				if(i['Mes'] == model.get_value(it, 1)):
					data.remove(i)
					
			save_files(data)
			
			self.remove_all_data()
			self.show_all_data()

	def remove_all_data(self):
		if len (self.listmodel) != 0:
			for i in range(len(self.listmodel)):
				iter = self.listmodel.get_iter(0)
				self.listmodel.remove(iter)
				
		print ("Empty list")


#Ventana Permiso denegado
class Ventana_permiso():

	def __init__(self):
		print("permiso denegado")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")

		self.ventana_denegada = self.builder.get_object("adv")

		self.boton_aceptar = self.builder.get_object("acepta")
		self.boton_aceptar.connect("clicked", self.aceptar)

		self.ventana_denegada.show_all()

	def aceptar(self, event):
		self.ventana_denegada.destroy()

#Ventana Arbitros
class Ventana_referis():

	def __init__(self):
		print("agregar arbitro")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyect.glade")
		self.ventanaxd = self.builder.get_object("añadir_arbitros")
		self.ventanaxd.set_title("Agregar Arbitros")
		self.ventanaxd.show_all()

		self.boton_aceptar1 = self.builder.get_object("Accept")
		self.boton_cancelar1 = self.builder.get_object("Cancell")
		self.boton_aceptar1.connect("clicked", self.aceptar1)
		self.boton_cancelar1.connect("clicked", self.cancelar1)

		self.nombre_arbitro = self.builder.get_object("nombrereferi")
		self.numero_peleas = self.builder.get_object("peleasreferi")
		self.edad_arbitro = self.builder.get_object("edadreferi")
		self.nacionalidad = self.builder.get_object("nacionalidadreferi")

	def aceptar1(self, btn=None):

		nombre = self.nombre_arbitro.get_text()
		peleas_arbitradas = self.numero_peleas.get_text()
		edad = self.edad_arbitro.get_text()
		nacionalidad = self.nacionalidad.get_text()

		j = { "Nombre Arbitro": nombre,
				"Pelas Arbitradas": peleas_arbitradas,
				"Edad": edad,
				"Nacionalidad": nacionalidad}

		f = open_files2()
		f.append(j)
		save_files2(f)
		self.ventanaxd.destroy()

	def cancelar1(self, btn=None):
		self.ventanaxd.destroy()


#main
if __name__ == "__main__":
	p = Ventana_inicio()
	Gtk.main()
